package com.demo.gamesimulator.players.service;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@EnableKafkaStreams
@EnableKafka
public class PlayersScoreGenerator {
    @Value("${bootstrap.servers}") private String bootStrap;

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public StreamsConfig kStreamsConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "player-stream");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,  Serdes.Integer().getClass().getName());
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class.getName());
        props.put("bootstrap.servers", bootStrap);
        return new StreamsConfig(props);
    }

    @Bean
    public KStream<String, Integer> kStream(StreamsBuilder kStreamBuilder) {
        Random rand  = new Random();
        KStream<String, Integer> stream = kStreamBuilder.stream("game-seed");
        stream.
                map((k, v) -> new KeyValue<>(k,  rand.nextInt(2*v)))
                .through("player1-guess")
                .map((k, v) -> new KeyValue<>(k, rand.nextInt(v + 20)))
                .through("player2-guess")
                .map((k, v) -> new KeyValue<>(k, rand.nextInt(v + 40)))
                .through("player3-guess");
        return stream;
    }
}
